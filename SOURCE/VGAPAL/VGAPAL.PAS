{

MIT License

Copyright (c) 2021 Jerome Shidel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

}

program VGAPAL;

uses QCrt, QStrings;

{$I VGABIOS.INC}

const
    Version : String = '0.10 (beta)';
    Year : String[4] = '2021';
    CRLF : String[2] = #$0d + #$0a;

var
    Palette : TPalette;
    DAC     : TDAC;

    procedure Help;
    const
        Padding = 12;
    begin
        WriteLn('VGA Text Mode Palette Utility ', Version, CRLF,
            '(c) ', Year, ' Jerome Shidel', CRLF);
        WriteLn('usage: ', Copy(ParamStr(0), LastPos('\', ParamStr(0)) + 1,
            Length(ParamStr(0))), ' [options]', CRLF);
        WriteLn(Space(4), RSpace('/?', Padding), 'Display help and exit');
        WriteLn;
        WriteLn(Space(4), RSpace('/T', Padding), 'Display color palette table');
        WriteLn;
        WriteLn(Space(4), RSpace('/I', Padding), 'Enable background intensity');
        WriteLn(Space(4), RSpace('/B', Padding), 'Enable blinking text');
        WriteLn;
        WriteLn(Space(4), RSpace('/R n', Padding),
            'Read color palette n (0-15)');
        WriteLn(Space(4), RSpace('/W n r g b', Padding),
            'Read color palette n (0-15). r, g and b (0-63)');
        WriteLn;
        WriteLn(Space(4), RSpace('/F n', Padding),
            'Fade to black and clear screen, with n (0-5) step slow down');
        WriteLn;
        Halt(0);
    end;

    procedure PrintTable;
    var
        A, I : integer;
    begin
        A := TextAttr;
        for I := 0 to 15 do begin
            Write(
                Space(1), 'Color:',
                Space(1),
                RSpace(IntStr(I),2),
                Space(4), 'Palette Value: 0x', HexByte(Palette.Palette[I]),
                Space(4), 'RGB Value:',
                Space(1),
                HexByte(DAC[Palette.Palette[I]].R), ',',
                HexByte(DAC[Palette.Palette[I]].G), ',',
                HexByte(DAC[Palette.Palette[I]].B),
                Space(4));
            TextColor(I);
            if (DAC[Palette.Palette[I]].R + DAC[Palette.Palette[I]].G +
                DAC[Palette.Palette[I]].B < $10) then
                TextBackground(LightGray);
            Write(Space(4), 'DEMO TEXT', Space(4));
            TextAttr := A;
            WriteLn;
        end;
    end;

    procedure PrintDemo;
    var
        A, X, Y, R : integer;
    begin
        A := TextAttr;
        { if GetBlink then R := 7 else }
        R := 15;
        for Y := 0 to R do begin
            for X := 0 to 15 do begin
                TextColor(X);
                TextBackground(Y);
                Write(CSpace(HexByte(Y * 16 + X), 4));
                TextAttr := A;
                if X < 15 then Write(Space(1));
            end;
            WriteLn;
        end;
    end;

    function StrToInt(Value : String) : integer;
    var
        I, E : integer;
    begin
        if LCase(Copy(Value, 1, 2)) = '0x' then begin
            Delete(Value, 1, 2);
            Value := '$' + Value;
        end;
        if  LCase(Copy(Value, Length(Value), 1)) = 'h' then begin
            Delete(Value, Length(Value), 1);
            Value := '$' + Value;
        end;
        Val(Value, I, E);
        if E <> 0 then I := -1;
        StrToInt := I;
    end;

    procedure InvalidColor(Value : String);
    begin
        WriteLn('ERROR: Color value "', Value, '" not between 0-15');
        Halt(1);
    end;

    procedure InvalidRGB(Value : String);
    begin
        WriteLn('ERROR: RGB value "', Value, '" not between 0-63');
        Halt(1);
    end;

    procedure PrintPalette(Color : String);
    var
        C : integer;
    begin
        C := StrToInt(Color);
        if (C < 0) or (C > 15) then InvalidColor(Color);
        WriteLn(
            '0x', HexByte(DAC[Palette.Palette[C]].R), ' ',
            '0x', HexByte(DAC[Palette.Palette[C]].G), ' ',
            '0x', HexByte(DAC[Palette.Palette[C]].B)
            );
    end;

    procedure SetColorRGB(Color, Red, Green, Blue : String);
    var
        C, R, G, B : integer;
    begin
        C := StrToInt(Color);
        if (C < 0) or (C > 15) then InvalidColor(Color);
        R := StrToInt(Red);
        if (R < 0) or (R > 63) then InvalidRGB(Red);
        G := StrToInt(Green);
        if (R < 0) or (R > 63) then InvalidRGB(Green);
        B := StrToInt(Blue);
        if (R < 0) or (R > 63) then InvalidRGB(Blue);
        DAC[Palette.Palette[C]].R := R;
        DAC[Palette.Palette[C]].G := G;
        DAC[Palette.Palette[C]].B := B;
        WriteDAC(0, 255, DAC);
    end;

    function IsOption (Param : integer; ShortOpt, LongOpt : String ) : boolean;
    begin
        IsOption := True;
        if ShortOpt <> '' then ShortOpt := '/' + LCase(ShortOpt);
        if LongOpt <> '' then LongOpt := '/' + LCase(LongOpt);
        if (LCase(ParamStr(Param)) = ShortOpt) or
            (LCase(ParamStr(Param)) = LongOpt) then
                Exit;
        IsOption := False;
    end;

    function HasOption ( ShortOpt, LongOpt : String ) : boolean;
    var
        I : integer;
    begin
        HasOption := True;
        for I := 1 to ParamCount do
            if (IsOption(I, ShortOpt, LongOpt)) then Exit;
        HasOption := False;
    end;

    procedure FadeOut(Slow : String);
    var
        I : Integer;
    begin
        I := StrToInt(Slow);
        if I < 0 then I := 0;
        if I > 4 then I := 5;
        FadeOutDAC(0, 255, DAC, I);
        ClrScr;
        WriteDAC(0, 255, DAC);
    end;

    procedure Main;
    var
        I : integer;
    begin
        I := 1;
        While I <= ParamCount do begin
            if IsOption(I, 'I', '') then SetBlink(False) else
            if IsOption(I, 'B', '') then SetBlink(True) else
            if IsOption(I, 'D', '') then PrintDemo else
            if IsOption(I, 'T', '') then PrintTable else
            if IsOption(I, 'F', '') then begin
                Inc(I);
                FadeOut(ParamStr(I));
            end else
            if IsOption(I, 'R', '') then begin
                Inc(I);
                PrintPalette(ParamStr(I));
            end else
            if IsOption(I, 'W', '') then begin
                SetColorRGB(
                    ParamStr(I+1),
                    ParamStr(I+2),
                    ParamStr(I+3),
                    ParamStr(I+4)
                    );
                I := I + 4;
            end;
            Inc(I);
        end;
    end;

begin
    if HasOption('?', '') then Help;
    GetPalette(Palette);
    ReadDAC(0, 255, DAC);
    Main;
    if ParamCount = 0 then PrintTable;
end.