{

MIT License

Copyright (c) 2021 Jerome Shidel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

}

type
	TRGB = record
		R, G, B : byte;
	end;
	TDAC = array[0..255] of TRGB;
	TPalette = record
		Palette : array[0..15] of byte;
		Overscan : Byte;
	end;

procedure ReadDAC( First, Count : word; var Buffer ); assembler;
asm
	MOV AX, 1017h
	MOV BX, First
	MOV CX, Count
	LES DX, Buffer
	INT 10h
end;

procedure WriteDAC( First, Count : word; var Buffer ); assembler;
asm
	MOV AX, 1012h
	MOV BX, First
	MOV CX, Count
	LES DX, Buffer
	INT 10h
end;

procedure FadeOutDAC( First, Count : word; var Buffer; Slow : byte );
var
	Temp : TDAC;
	I, J, X : integer;
	LastTimer : LongInt;
	TimerTick : LongInt absolute $0040:$006c;
begin
	LastTimer := TimerTick;
	While LastTimer = TimerTick do;
	for I := 1 to 8 do begin
		for J := First to First + Count - 1 do begin
			Temp[J].R := TDAC(Buffer)[J].R shr I;
			Temp[J].G := TDAC(Buffer)[J].G shr I;
			Temp[J].B := TDAC(Buffer)[J].B shr I;
		end;
		WriteDAC(First, Count, Temp);
		for X := 0 to Slow do begin
    		LastTimer := TimerTick;
	    	While LastTimer = TimerTick do;
		end;
	end;
end;

procedure FadeInDAC( First, Count : word; var Buffer; Slow : byte );
var
	Temp : TDAC;
	I, J, X : integer;
	LastTimer : LongInt;
	TimerTick : LongInt absolute $0040:$006c;
begin
	LastTimer := TimerTick;
	While LastTimer = TimerTick do;
	for I := 1 to 8 do begin
		for J := First to First + Count - 1 do begin
			Temp[J].R := TDAC(Buffer)[J].R shr (8 - I);
			Temp[J].G := TDAC(Buffer)[J].G shr (8 - I);
			Temp[J].B := TDAC(Buffer)[J].B shr (8 - I);
		end;
		WriteDAC(First, Count, Temp);
		for X := 0 to Slow do begin
    		LastTimer := TimerTick;
	    	While LastTimer = TimerTick do;
		end;
	end;
end;

procedure SetPalette(var Buffer); assembler;
asm
	XOR BX, BX
	MOV AX, 1002h
	LES DX, Buffer
	INT 10h
end;

procedure GetPalette(var Buffer); assembler;
asm
	LES  DX, Buffer
	MOV	 CX, 17
	MOV  BX, DX
	MOV  AL, 0FFh
@@Clear:
	MOV	  ES:[BX], AL
	INC   BX
	LOOP  @@Clear
	MOV  AX, 1009h
	INT 10h
end;

procedure ReadState ( var Buffer ); assembler;
asm
	MOV AX, 1B00h
	XOR BX, BX
	LES DI, Buffer
	INT 10h
end;

function GetBlink : boolean;
var
    Info : array[0..63] of byte;
begin
    ReadState(Info);
    GetBlink := Info[$20] and $20 = $20; { 00100000 }
end;

procedure SetBlink(Enabled : boolean); assembler;
asm
    MOV CL, Enabled
    XOR BX, BX
	MOV AX, 1003h
	CMP CL, False
	JE  @@SetState
	MOV BL, 01h
@@SetState:
	INT 10h
end;


